using PostgresqlDemo.DataAccess.Entities;
using Core.AutoMapper;

namespace PostgresqlDemo.WebCore.AspNetIdentity
{
    public class LoggedInUser : IMapFrom<User>
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Avatar { get; set; }
    }
}