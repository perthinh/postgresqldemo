using System.Security.Claims;
using System.Threading.Tasks;
using PostgresqlDemo.DataAccess.AspNetIdentity;
using PostgresqlDemo.DataAccess.Entities;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace PostgresqlDemo.WebCore.AspNetIdentity
{
    public class SignInManager : SignInManager<User, int>
    {
        public SignInManager(UserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(User user)
        {
            return user.GenerateUserIdentityAsync((UserManager)UserManager);
        }
    }
}