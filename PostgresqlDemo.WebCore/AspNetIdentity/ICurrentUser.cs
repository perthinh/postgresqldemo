﻿using System.Collections.Generic;
using PostgresqlDemo.DataAccess.Enums;

namespace PostgresqlDemo.WebCore.AspNetIdentity
{
    public interface ICurrentUser
    {
        int Id { get; }
        int TenantId { get; }
        LoggedInUser Info { get; }
        IList<string> Roles { get; }
        bool IsInRole(params ERole[] roles);
        void Refresh();
    }
}