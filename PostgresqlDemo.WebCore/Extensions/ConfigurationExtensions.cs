﻿using Core.Configuration;

namespace PostgresqlDemo.WebCore.Extensions
{
    public static class ConfigurationExtensions
    {
        public static string UploadFolder(this ISettingManager setting)
        {
            return setting.GetNotEmptyValue("UploadFolder").TrimEnd('/');
        }
    }
}
