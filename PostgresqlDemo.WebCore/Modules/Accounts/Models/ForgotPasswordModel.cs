﻿using System.ComponentModel.DataAnnotations;

namespace PostgresqlDemo.WebCore.Modules.Accounts.Models
{
    public class ForgotPasswordModel
    {
        [Required]
        [EmailAddress, StringLength(255)]
        public string Email { get; set; }
    }
}
