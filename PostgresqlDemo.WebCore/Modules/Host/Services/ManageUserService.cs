﻿using System.Linq;
using PostgresqlDemo.DataAccess.AspNetIdentity;
using PostgresqlDemo.DataAccess.Repositories;
using PostgresqlDemo.WebCore.Modules.Host.Models;
using Core.AutoMapper;
using Core.DataAccess.Uow;
using Core.Net.Mail.Template;

namespace PostgresqlDemo.WebCore.Modules.Host.Services
{
    public interface IHostUserService
    {
        IQueryable<HostUserPreview> GetList();
    }

    public class ManageUserService : UserService, IHostUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private UserRepository UserRepo => _unitOfWork.Repo<UserRepository>();

        public ManageUserService(
            IUnitOfWork unitOfWork,
            ITemplateEmailSender emailSender,
            UserManager userManager) : base(emailSender, userManager)
        {
            _unitOfWork = unitOfWork;
        }

        public IQueryable<HostUserPreview> GetList()
        {
            return UserRepo.Queryable()
                .ProjectTo<HostUserPreview>();
        }
    }
}