﻿using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using Core.Web.MetadataModel.Validations;

namespace PostgresqlDemo.WebCore.Modules.FileManager.Models
{
    public class UploadedFileModel
    {
        [HiddenInput]
        [Required]
        public string TargetId { get; set; }

        [ImageUpload]
        public HttpPostedFileBase FileUpload { get; set; }
    }
}