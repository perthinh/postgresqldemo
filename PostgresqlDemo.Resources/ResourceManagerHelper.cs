﻿using System.Collections.Generic;
using System.Resources;

namespace PostgresqlDemo.Resources
{
    public static class ResourceManagerHelper
    {
        public static IList<ResourceManager> GetAll()
        {
            return new[] {
                ResTexts_en.ResourceManager,
                ResTexts_vi.ResourceManager
            };
        }
    }
}
