﻿using System;

namespace Core.Web.Caching
{
    /// <summary>
    /// User-level caching (user session)
    /// </summary>
    public interface ISessionCache
    {
        void Set(string key, object value);
        bool Contains(string key);
        void Clear();
        object Get(string key);
        T Get<T>(string key);
        T GetOrAdd<T>(string key, T value);
        T GetOrAdd<T>(string key, Func<T> value);
        void Remove(string key);
    }
}
