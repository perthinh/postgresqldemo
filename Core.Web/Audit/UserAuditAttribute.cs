﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;
using Core.Auditing;
using Core.Dependency;
using Core.Extensions;
using Core.Runtime.Session;
using Core.Timing;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Serilog;

namespace Core.Web.Audit
{
    public class UserAuditAttribute : ActionFilterAttribute
    {
        private static IAuditingConfig Config => DependencyManager.GetInstance<IAuditingConfig>();
        private static IAuditingStore AuditingStore => DependencyManager.GetInstance<IAuditingStore>();
        private static IUserSession UserSession => DependencyManager.GetInstance<IUserSession>();

        private readonly ILogger _logger;
        private AuditInfo _auditInfo;
        private Stopwatch _actionStopwatch;

        public UserAuditAttribute()
        {
            _logger = Log.ForContext<UserAuditAttribute>();
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HandleAuditingBeforeAction(filterContext);
            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            HandleAuditingAfterAction(filterContext);
        }

        private void HandleAuditingBeforeAction(ActionExecutingContext filterContext)
        {
            if (!ShouldSaveAudit(filterContext))
            {
                _auditInfo = null;
                return;
            }

            _actionStopwatch = Stopwatch.StartNew();
            _auditInfo = new AuditInfo
            {
                UserId = UserSession.UserId,
                ServiceName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                MethodName = filterContext.ActionDescriptor.ActionName,
                Parameters = ConvertArgumentsToJson(filterContext.ActionParameters),
                ExecutionTime = Clock.Now
            };
        }

        private void HandleAuditingAfterAction(ActionExecutedContext filterContext)
        {
            if (_auditInfo == null || _actionStopwatch == null)
            {
                return;
            }

            _actionStopwatch.Stop();

            _auditInfo.ExecutionDuration = _actionStopwatch.Elapsed.TotalMilliseconds.ChangeTypeTo<int>(0);
            _auditInfo.Exception = filterContext.Exception;
            _auditInfo.ClientIpAddress = filterContext.RequestContext.HttpContext.Request.UserHostAddress;

            AuditingStore.SaveAsync(_auditInfo);
        }

        private bool ShouldSaveAudit(ActionExecutingContext filterContext)
        {
            if (Config == null)
            {
                return false;
            }

            if (!Config.IsEnabled)
            {
                return false;
            }

            if (HasDisableAuditingAttribute(filterContext.ActionDescriptor))
            {
                return false;
            }

            return AuditingHelper.ShouldSaveAudit(Config, UserSession);
        }

        private static bool HasDisableAuditingAttribute(ActionDescriptor actionDescriptor)
        {
            if (actionDescriptor.ControllerDescriptor.GetCustomAttributes(typeof(DisableAuditingAttribute), false).Any())
            {
                return true;
            }

            if (actionDescriptor.GetCustomAttributes(typeof(DisableAuditingAttribute), false).Any())
            {
                return true;
            }

            return false;
        }

        private string ConvertArgumentsToJson(IDictionary<string, object> arguments)
        {
            try
            {
                if (!arguments.Any())
                {
                    return "{}";
                }

                return JsonConvert.SerializeObject(arguments, new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });
            }
            catch (Exception ex)
            {
                _logger.Warning(ex, "Could not serialize arguments for method: " + _auditInfo.ServiceName + "." + _auditInfo.MethodName);
                return "{}";
            }
        }
    }
}
