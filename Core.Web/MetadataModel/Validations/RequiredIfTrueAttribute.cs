﻿using System.ComponentModel.DataAnnotations;

namespace Core.Web.MetadataModel.Validations
{
    public class RequiredIfTrueAttribute : ValidationAttribute
    {
        public string OtherProperty { get; }

        public RequiredIfTrueAttribute(string otherProperty)
        {
            OtherProperty = otherProperty;
            ErrorMessage = "{0} is required";
        }

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            var otherPropValue = (bool)context.ObjectInstance.GetType().GetProperty(OtherProperty).GetValue(context.ObjectInstance, null);
            if (otherPropValue && string.IsNullOrEmpty(value?.ToString()))
            {
                return new ValidationResult(FormatErrorMessage(context.DisplayName));
            }
            return ValidationResult.Success;
        }
    }
}
