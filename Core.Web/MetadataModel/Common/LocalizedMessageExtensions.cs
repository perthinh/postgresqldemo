﻿using Core.Extensions;
using Core.Localization.Sources;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Core.Web.MetadataModel.Common
{
    public static class LocalizedMessageExtensions
    {
        public static string ErrorMessage<TAttribute>(this ILocalizationSource source, ModelMetadata metadata, TAttribute attribute, params object[] parameters)
            where TAttribute : ValidationAttribute
        {
            var errorMessasge = GetMetadataErrorMessasge<TAttribute>(source, metadata);
            return string.IsNullOrWhiteSpace(errorMessasge) ? source.ErrorMessage(attribute, parameters) : errorMessasge;
        }

        public static string FormatErrorMessage<TAttribute>(this ILocalizationSource source, ModelMetadata metadata, TAttribute attribute, params object[] parameters)
            where TAttribute : ValidationAttribute
        {
            var errorMessasge = GetMetadataErrorMessasge<TAttribute>(source, metadata);
            return string.IsNullOrWhiteSpace(errorMessasge) ? source.FormatErrorMessage(attribute, parameters) : errorMessasge;
        }

        private static string GetMetadataErrorMessasge<TAttribute>(ILocalizationSource source, ModelMetadata metadata)
          where TAttribute : ValidationAttribute
        {
            return source.GetModelStringOrNull(metadata.ContainerType,
                 metadata.PropertyName,
                 typeof(TAttribute).Name.TrimEnd("Attribute"));
        }
    }
}
