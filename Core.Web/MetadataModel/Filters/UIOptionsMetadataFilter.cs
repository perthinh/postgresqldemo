﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Core.Web.MetadataModel.Attributes;
using Core.Web.MetadataModel.Common;

namespace Core.Web.MetadataModel.Filters
{
    public class UIOptionsMetadataFilter : IModelMetadataFilter
    {
        public void TransformMetadata(ModelMetadata metadata, IReadOnlyCollection<Attribute> attributes)
        {
            var options = metadata.Options();
            options.Common = attributes.OfType<UIOptionsAttribute>().FirstOrDefault() ?? new UIOptionsAttribute();
            options.Date = attributes.OfType<DateOptionsAttribute>().FirstOrDefault() ?? new DateOptionsAttribute();
        }
    }
}