﻿using Core.Web.MetadataModel.Common;
using Core.Web.MetadataModel.Transfomers;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Core.Web.MetadataModel.Filters
{
    /// <summary>
    /// Convert model data annatations to html attributes
    /// </summary>
    public class AttributeTransformationFilter : IModelMetadataFilter
    {
        public void TransformMetadata(ModelMetadata metadata, IReadOnlyCollection<Attribute> attributes)
        {
            if (string.IsNullOrWhiteSpace(metadata.PropertyName)) return;

            var dict = new HtmlAttributeDictionary();

            foreach (var attribute in attributes)
            {
                AttributeTransformers.Create(attribute).Transform(metadata, attribute, dict);
            }

            metadata.AdditionalAttributes().Merge(dict);
        }
    }
}