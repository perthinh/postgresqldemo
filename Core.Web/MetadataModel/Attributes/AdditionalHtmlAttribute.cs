﻿using System;
using System.Web.Mvc;
using Core.Web.MetadataModel.Common;

namespace Core.Web.MetadataModel.Attributes
{
    public abstract class AdditionalHtmlAttribute : Attribute, IHaveAdditionalHtml
    {
        public abstract void AddAttributes(ModelMetadata metadata, HtmlAttributeDictionary dict);
    }
}