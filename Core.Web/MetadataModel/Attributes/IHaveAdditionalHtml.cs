﻿using System.Web.Mvc;
using Core.Web.MetadataModel.Common;

namespace Core.Web.MetadataModel.Attributes
{
    public interface IHaveAdditionalHtml
    {
        void AddAttributes(ModelMetadata metadata, HtmlAttributeDictionary dict);
    }
}