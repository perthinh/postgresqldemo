﻿using Core.Web.MetadataModel.Common;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Core.Web.MetadataModel.Transfomers
{
    public class PhoneTransformer : GenericTransformer<PhoneAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, PhoneAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.AddRuleMessage("mask", ErrorMessage(metadata, attribute));
        }
    }
}