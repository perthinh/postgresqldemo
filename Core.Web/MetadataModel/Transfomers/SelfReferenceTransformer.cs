﻿using System.Web.Mvc;
using Core.Web.MetadataModel.Common;
using Core.Web.MetadataModel.Validations;

namespace Core.Web.MetadataModel.Transfomers
{
    public class SelfReferenceTransformer : GenericTransformer<SelfReferenceAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, SelfReferenceAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.Data("ref", $"#{attribute.OtherProperty}");
            dict.AddRuleMessage("selfref", ErrorMessage(metadata, attribute));
        }
    }
}