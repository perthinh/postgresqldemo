using Core.Web.MetadataModel.Common;
using System.Web.Mvc;
using Core.Web.MetadataModel.Validations;

namespace Core.Web.MetadataModel.Transfomers
{
    public class RegexPatternTransformer : GenericTransformer<RegexPatternAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, RegexPatternAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.Data("pattern", attribute.Pattern);
            dict.AddRuleMessage("pattern", ErrorMessage(metadata, attribute));
        }
    }
}