﻿using System.Web.Mvc;
using Core.Web.MetadataModel.Common;
using Core.Web.MetadataModel.Validations;

namespace Core.Web.MetadataModel.Transfomers
{
    public class ImageUploadTransformer : GenericTransformer<ImageUploadAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, ImageUploadAttribute attribute, HtmlAttributeDictionary dict)
        {
            if (attribute.IsRequired)
            {
                dict.Data("upload", "required");
                dict.AddRuleMessage("upload", ErrorMessage(metadata, attribute));
            }
        }
    }
}