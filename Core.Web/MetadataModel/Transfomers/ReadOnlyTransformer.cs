using Core.Web.MetadataModel.Common;
using System.ComponentModel;
using System.Web.Mvc;

namespace Core.Web.MetadataModel.Transfomers
{
    public class ReadOnlyTransformer : GenericTransformer<ReadOnlyAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, ReadOnlyAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.Attr("readonly", "");
        }
    }
}