﻿using System.Web.Mvc;
using Core.Web.MetadataModel.Common;
using Core.Web.MetadataModel.Validations;

namespace Core.Web.MetadataModel.Transfomers
{
    public class BooleanRequiredTransformer : GenericTransformer<BooleanRequiredAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, BooleanRequiredAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.Data("boolean", "required");
            dict.AddRuleMessage("boolRequired", ErrorMessage(metadata, attribute));
        }
    }
}
