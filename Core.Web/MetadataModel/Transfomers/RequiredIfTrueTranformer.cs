﻿using System.Web.Mvc;
using Core.Web.MetadataModel.Common;
using Core.Web.MetadataModel.Validations;

namespace Core.Web.MetadataModel.Transfomers
{
    public class RequiredIfTrueTranformer : GenericTransformer<RequiredIfTrueAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, RequiredIfTrueAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.Data("otherprop", $"#{attribute.OtherProperty}");
            dict.AddRuleMessage("deprequiredif", ErrorMessage(metadata, attribute));
        }
    }
}
