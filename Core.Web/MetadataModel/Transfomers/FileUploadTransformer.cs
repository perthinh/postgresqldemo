﻿using System.Web.Mvc;
using Core.Web.MetadataModel.Common;
using Core.Web.MetadataModel.Validations;

namespace Core.Web.MetadataModel.Transfomers
{
    public class FileUploadTransformer : GenericTransformer<FileUploadAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, FileUploadAttribute attribute, HtmlAttributeDictionary dict)
        {
            if (attribute.IsRequired)
            {
                dict.Data("upload", "required");
                dict.AddRuleMessage("upload", ErrorMessage(metadata, attribute));
            }
        }
    }
}