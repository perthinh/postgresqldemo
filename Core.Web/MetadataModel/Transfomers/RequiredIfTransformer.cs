﻿using System.Web.Mvc;
using Core.Web.MetadataModel.Common;
using Core.Web.MetadataModel.Validations;

namespace Core.Web.MetadataModel.Transfomers
{
    public class RequiredIfTransformer : GenericTransformer<RequiredIfAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, RequiredIfAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.Data("otherprop", $"#{attribute.OtherProperty}");
            dict.AddRuleMessage("deprequired", ErrorMessage(metadata, attribute));
        }
    }
}
