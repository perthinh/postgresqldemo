using Core.Web.MetadataModel.Common;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Core.Web.MetadataModel.Transfomers
{
    public class EmailAddressTransformer : GenericTransformer<EmailAddressAttribute>
    {
        protected override void SafeTransform(ModelMetadata metadata, EmailAddressAttribute attribute, HtmlAttributeDictionary dict)
        {
            dict.AddRuleMessage("email", ErrorMessage(metadata, attribute));
        }
    }
}