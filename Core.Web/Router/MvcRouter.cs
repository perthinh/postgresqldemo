﻿using System;
using System.Collections.Generic;
using Core.Web.Router.Providers;
using CuttingEdge.Conditions;

namespace Core.Web.Router
{
    public class MvcRouter
    {
        private static readonly Lazy<MvcRouter> Instance = new Lazy<MvcRouter>(RouteProviderManager.Current.Load);
        public static MvcRouter Current => Instance.Value;

        private readonly Dictionary<string, MvcRoute> _dict = new Dictionary<string, MvcRoute>();

        public MvcRouter(MvcRoute rootNode)
        {
            Condition.Requires(rootNode, nameof(rootNode)).IsNotNull();
            RootNode = rootNode;
        }

        public MvcRoute RootNode { get; }

        public MvcRoute CurrentNode
        {
            get
            {
                var key = KeyGenerator.FromCurrentRoute();
                return _dict.ContainsKey(key) ? _dict[key] : MvcRoute.UntitledNode;
            }
        }

        internal void Build()
        {
            RootNode.Router = this;
            foreach (var node in RootNode.ChildNodes)
            {
                node.OverrideArea();
                BuildRecursive(node);
            }
        }

        private void BuildRecursive(MvcRoute parent)
        {
            parent.Router = this;
            _dict.Add(KeyGenerator.FromSiteMapNode(parent), parent);

            foreach (var node in parent.ChildNodes)
            {
                BuildRecursive(node);
            }
        }
    }
}