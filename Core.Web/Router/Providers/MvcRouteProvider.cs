﻿using System;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Xml.Linq;

namespace Core.Web.Router.Providers
{
    public class MvcRouteProvider : AbstractMvcRouteProvider
    {
        public override MvcRouter Load()
        {
            var path = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "sitemap.xml");
            var xdoc = XDocument.Parse(File.ReadAllText(path));

            if (xdoc.Root == null)
            {
                throw new ArgumentNullException("Root");
            }

            var rootElement = xdoc.Root.Elements().FirstOrDefault();
            if (rootElement == null)
            {
                throw new ArgumentNullException("siteMapRoot");
            }

            var rootNode = rootElement.ToSiteMapNode();
            GetChildNodes(rootNode, rootElement);

            return BuildRouter(rootNode);
        }

        private void GetChildNodes(MvcRoute parent, XContainer parentElement)
        {
            foreach (var element in parentElement.Elements())
            {
                var child = element.ToSiteMapNode();
                parent.AddChild(child);
                GetChildNodes(child, element);
            }
        }
    }
}