using System;

namespace Core.Web.Router.Providers
{
    public static class RouteProviderManager
    {
        public static IRouteProvider Current { get; private set; } = new MvcRouteProvider();

        public static void SetProvider(Func<IRouteProvider> funcProvider)
        {
            Current = funcProvider();
        }
    }
}