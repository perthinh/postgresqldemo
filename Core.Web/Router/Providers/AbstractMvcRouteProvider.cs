namespace Core.Web.Router.Providers
{
    public abstract class AbstractMvcRouteProvider : IRouteProvider
    {
        public abstract MvcRouter Load();

        protected virtual MvcRouter BuildRouter(MvcRoute rootNode)
        {
            var router = new MvcRouter(rootNode);
            router.Build();
            return router;
        }
    }
}