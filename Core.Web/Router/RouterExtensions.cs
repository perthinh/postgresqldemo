﻿using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Xml.Linq;
using Core.Extensions;

namespace Core.Web.Router
{
    public static class RouterExtensions
    {
        public static string GetSiteMapUrl(this HtmlHelper htmlHelper, MvcRoute node)
        {
            if (!node.Linkable) return "#";

            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            return urlHelper.Action(node.Action, node.Controller, new { area = node.Area });
        }

        public static bool IsInCurrentPath(this MvcRoute node)
        {
            var currentNode = node.Router.CurrentNode;
            return node == currentNode ||
                   currentNode.GetParentNodes().Any(parent => node == parent);
        }

        public static string ActiveClass(this MvcRoute node)
        {
            return node.IsInCurrentPath() ? "active" : string.Empty;
        }

        internal static MvcRoute ToSiteMapNode(this XElement element)
        {
            var node = new MvcRoute
            {
                Controller = element.GetAttributeValue("controller"),
                Action = element.GetAttributeValue("action", "Index"),
                Area = element.GetAttributeValue("area", null),
                Icon = element.GetAttributeValue("icon"),
                Description = element.GetAttributeValue("description"),
                Title = element.GetAttributeValue("title"),
                Visible = element.GetAttributeValue("visible", true),
            };

            node.Linkable = !string.IsNullOrWhiteSpace(node.Controller) &&
                            element.GetAttributeValue("linkable", true);

            return node;
        }

        private static T GetAttributeValue<T>(this XElement element, string attribute, T defaultValue = default(T))
        {
            return element.GetAttributeValue(attribute).ChangeTypeTo<T>(defaultValue);
        }

        private static string GetAttributeValue(this XElement element, string attribute, string defaultValue = "")
        {
            var attr = element.Attribute(attribute);
            return attr?.Value ?? defaultValue;
        }

        public static string ToFriendly(this int id, string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                var unsignString = name;
                var signs = new string[]
                {
                    "aAeEoOuUiIdDyY",
                    "áàạảãâấầậẩẫăắằặẳẵ",
                    "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
                    "éèẹẻẽêếềệểễ",
                    "ÉÈẸẺẼÊẾỀỆỂỄ",
                    "óòọỏõôốồộổỗơớờợởỡ",
                    "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
                    "úùụủũưứừựửữ",
                    "ÚÙỤỦŨƯỨỪỰỬỮ",
                    "íìịỉĩ",
                    "ÍÌỊỈĨ",
                    "đ",
                    "Đ",
                    "ýỳỵỷỹ",
                    "ÝỲỴỶỸ"
                };
                for (var i = 1; i < signs.Length; i++)
                {
                    for (int j = 0; j < signs[i].Length; j++)
                    {
                        unsignString = unsignString.Replace(signs[i][j], signs[0][i - 1]);
                    }
                }

                string phrase = $"{id}-{unsignString}";

                var bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(phrase);
                var str = System.Text.Encoding.ASCII.GetString(bytes).ToLower();

                // invalid chars           
                str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
                // convert multiple spaces into one space   
                str = Regex.Replace(str, @"\s+", " ").Trim();
                // cut and trim 
                str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
                str = Regex.Replace(str, @"\s", "-"); // hyphens   
                return str;
            }

            return "";
        }
    }
}