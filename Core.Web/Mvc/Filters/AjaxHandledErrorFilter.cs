﻿using System;
using System.Web.Mvc;
using Core.Web.Mvc.Ajax;

namespace Core.Web.Mvc.Filters
{
    public class AjaxHandledErrorFilter : FilterAttribute, IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext == null)
                throw new ArgumentNullException(nameof(filterContext));

            if (filterContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
#if DEBUG
                filterContext.Result = new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = AjaxResult.Error().WithException(filterContext.Exception)
                                     .ToResult()
                };
#else
                filterContext.Result = new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = AjaxResult.Error().WithMessages($"An error has occurred at {Core.Timing.Clock.Now}")
                                     .ToResult()
                };
#endif

                filterContext.ExceptionHandled = true;

                filterContext.HttpContext.Response.Clear();
                filterContext.HttpContext.Response.StatusCode = 500;
                filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
            }
        }
    }
}
