﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Core.Exceptions;
using Core.Extensions;

namespace Core.Web.Mvc.Models
{
    public static class ModelStateExtensions
    {
        public static void AddException(this ModelStateDictionary dict, Exception ex)
        {
            string message;
            if (ex is BusinessException)
            {
                message = ex.Message;
            }
            else
            {
                message = ex.GetBaseErrorMessage();
            }
            dict.AddModelError("", message);
        }

        public static bool IsModelValid(this object source)
        {
            var validationResults = new List<ValidationResult>();
            return source.IsModelValid(validationResults);
        }

        public static bool IsModelValid(this object source, List<ValidationResult> results)
        {
            var context = new ValidationContext(source);
            return Validator.TryValidateObject(source, context, results, true);
        }
    }
}
