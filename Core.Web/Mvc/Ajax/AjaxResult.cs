using System.Collections.Generic;

namespace Core.Web.Mvc.Ajax
{
    public class AjaxResult
    {
        public EResultType ResultType { get; set; }
        public object Data { get; set; }
        public IList<string> Errors { get; set; }

        public AjaxResult()
        {
            Errors = new List<string>();
        }

        public void AddErrors(params string[] errors)
        {
            foreach (var error in errors)
            {
                Errors.Add(error);
            }
        }

        public static AjaxResultFluent Success()
        {
            return new AjaxResultFluent().Result(EResultType.Success);
        }

        public static AjaxResultFluent Error()
        {
            return new AjaxResultFluent().Result(EResultType.Error);
        }
    }
}