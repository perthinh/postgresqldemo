using Core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Core.Web.Mvc.Ajax
{
    public class AjaxResultFluent
    {
        private readonly AjaxResult _result = new AjaxResult();

        public AjaxResultFluent Result(EResultType type)
        {
            _result.ResultType = type;
            return this;
        }

        public AjaxResultFluent Data(object data)
        {
            _result.Data = data;
            return this;
        }

        public AjaxResultFluent WithException(Exception ex)
        {
            _result.Errors.Add(ex.GetBaseErrorMessage());
            return this;
        }

        public AjaxResultFluent WithMessages(params string[] errors)
        {
            foreach (var error in errors)
            {
                _result.Errors.Add(error);
            }
            return this;
        }

        public AjaxResultFluent WithModelState(ModelStateDictionary modelState)
        {
            foreach (var state in modelState)
            {
                AddErrors(state);
            }
            return this;
        }

        public AjaxResultFluent WithModelState(string key, ModelStateDictionary modelState)
        {
            foreach (var state in modelState.Where(x => x.Key == key))
            {
                AddErrors(state);
            }
            return this;
        }

        public AjaxResult ToResult()
        {
            return _result;
        }

        private void AddErrors(KeyValuePair<string, ModelState> state)
        {
            foreach (var error in state.Value.Errors)
            {
                _result.AddErrors(error.ErrorMessage);
            }
        }
    }
}