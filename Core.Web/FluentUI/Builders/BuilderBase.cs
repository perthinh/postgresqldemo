using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using Core.Web.FluentUI.Models;

namespace Core.Web.FluentUI.Builders
{
    public abstract class BuilderBase<TModel, TBuilder> : IHtmlString
        where TModel : ModelBase, new()
        where TBuilder : BuilderBase<TModel, TBuilder>
    {
        protected TModel Model = new TModel();

        public TBuilder Name(string name)
        {
            Model.Name = name;
            return (TBuilder)this;
        }

        public TBuilder HtmlAttributes(IDictionary<string, object> htmlAttributes)
        {
            Model.HtmlAttributes = htmlAttributes;
            return (TBuilder)this;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public string ToHtmlString()
        {
            return Model.ToHtmlString();
        }
    }
}