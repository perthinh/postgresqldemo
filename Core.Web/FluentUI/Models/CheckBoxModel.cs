﻿using Core.Web.Extensions;
using HtmlTags;

namespace Core.Web.FluentUI.Models
{
    public class CheckBoxModel : ModelBase
    {
        public bool Checked { get; set; }
        public bool Value { get; set; }
        public string DisplayName { get; set; }
        public bool ShowLabel { get; set; }

        protected override string Render()
        {
            var mainTag = new DivTag().AddClass("checkbox");

            var checkboxTag = new CheckboxTag(Checked)
                .NameAndId(Name)
                .AttrIfTrue(Value, "value", Value)
                .Attr(HtmlAttributes);

            var labelTag = new HtmlTag("label").Attr("for", Name).AppendHtmlIfTrue(ShowLabel, DisplayName);
            
            mainTag.Children.Add(checkboxTag);
            mainTag.Children.Add(labelTag);

            return mainTag.ToHtmlString();
        }
    }
}