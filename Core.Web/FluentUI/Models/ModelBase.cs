﻿using System.Collections.Generic;
using System.Web;

namespace Core.Web.FluentUI.Models
{
    public abstract class ModelBase : IHtmlString
    {
        public IDictionary<string, object> HtmlAttributes { get; set; }
        public string Name { get; set; }

        protected ModelBase()
        {
            HtmlAttributes = new Dictionary<string, object>();
        }

        public string ToHtmlString()
        {
            return ToString();
        }

        public override string ToString()
        {
            return Render();
        }

        protected abstract string Render();
    }
}