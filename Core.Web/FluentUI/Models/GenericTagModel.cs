﻿using Core.Web.Extensions;
using HtmlTags;

namespace Core.Web.FluentUI.Models
{
    public class GenericTagModel : ModelBase
    {
        public string TagName { get; set; }
        public string Text { get; set; }

        protected override string Render()
        {
            var tag = new HtmlTag(TagName)
                .Text(Text)
                .Attr(HtmlAttributes);
            return tag.ToHtmlString();
        }
    }
}