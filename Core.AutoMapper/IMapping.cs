namespace Core.AutoMapper
{
    public interface IMapping<T> : IMapTo<T>, IMapFrom<T>
    {

    }
}