using AutoMapper;

namespace Core.AutoMapper
{
    public interface ICustomMappings
    {
        void CreateMappings(IMapperConfigurationExpression configuration);

    }
}