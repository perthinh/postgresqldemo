﻿using CuttingEdge.Conditions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoMapper;

namespace Core.AutoMapper
{
    public static class AutoMapperConfigurator
    {
        public static void Configure(string assemblyPrefix, params string[] nsPrefixes)
        {
            Condition.Requires(assemblyPrefix, nameof(assemblyPrefix)).IsNotNullOrWhiteSpace();

            var types = AppDomain.CurrentDomain.GetAssemblies()
                .Where(x => x.FullName.StartsWith(assemblyPrefix))
                .SelectMany(x => x.GetExportedTypes())
                .Where(x => !nsPrefixes.Any() ||
                            (nsPrefixes.Any(prefix => x.Namespace != null && x.Namespace.StartsWith(prefix))))
                .ToList();

            Mapper.Initialize(cfg =>
            {
                CreateMapFromMappings(cfg, types);
                CreateMapToMappings(cfg, types);
                CreateCustomMappings(cfg, types);
            });
        }

        private static void CreateCustomMappings(IMapperConfigurationExpression cfg, IEnumerable<Type> types)
        {
            var maps = (from t in types
                        from i in t.GetInterfaces()
                        let tInfo = t.GetTypeInfo()
                        where typeof(ICustomMappings).IsAssignableFrom(t) &&
                              !tInfo.IsAbstract &&
                              !tInfo.IsInterface
                        select (ICustomMappings)Activator.CreateInstance(t)).ToArray();

            foreach (var map in maps)
            {
                map.CreateMappings(cfg);
            }
        }

        private static void CreateMapFromMappings(IMapperConfigurationExpression cfg, IEnumerable<Type> types)
        {
            var maps = FilterMappingTypes(types, typeof(IMapFrom<>));

            foreach (var map in maps)
            {
                cfg.CreateMap(map.GenericType, map.ModelType);
            }
        }

        private static void CreateMapToMappings(IMapperConfigurationExpression cfg, IEnumerable<Type> types)
        {
            var maps = FilterMappingTypes(types, typeof(IMapTo<>));

            foreach (var map in maps)
            {
                cfg.CreateMap(map.ModelType, map.GenericType);
            }
        }

        private static IEnumerable<MappingType> FilterMappingTypes(IEnumerable<Type> types, Type genericType)
        {
            return (from t in types
                    from i in t.GetInterfaces()
                    let tInfo = t.GetTypeInfo()
                    let iInfo = i.GetTypeInfo()
                    where iInfo.IsGenericType && i.GetGenericTypeDefinition() == genericType &&
                          !tInfo.IsAbstract &&
                          !tInfo.IsInterface
                    select new MappingType
                    {
                        GenericType = i.GetGenericArguments()[0],
                        ModelType = t
                    }).ToArray();
        }

        private class MappingType
        {
            public Type GenericType { get; set; }
            public Type ModelType { get; set; }
        }
    }
}
