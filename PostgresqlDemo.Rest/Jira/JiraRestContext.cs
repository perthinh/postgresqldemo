﻿using PostgresqlDemo.Rest.Infrastructure;
using PostgresqlDemo.Rest.Jira.Api;
using RestSharp.Authenticators;

namespace PostgresqlDemo.Rest.Jira
{
    public interface IJiraRestContext : IRestContext
    {
        BoardRest Boards { get; }
    }

    public class JiraRestContext : AbstractRestContext, IJiraRestContext
    {
        private const string JiraApiUrl = "https://PostgresqlDemo.atlassian.net/rest/agile/1.0/";

        public JiraRestContext()
            : base(JiraApiUrl)
        {
            Authenticator = new HttpBasicAuthenticator("PostgresqlDemo@gmail.com", "ThinhDuyx@X");
        }

        public BoardRest Boards => Rest<BoardRest>();
    }
}
