﻿using PostgresqlDemo.Rest.Infrastructure;
using PostgresqlDemo.Rest.Zendesk.Api;
using RestSharp.Authenticators;

namespace PostgresqlDemo.Rest.Zendesk
{
    public interface IZendeskRestContext : IRestContext
    {
        RequestRest Requests { get; }
    }

    public class ZendeskRestContext : AbstractRestContext, IZendeskRestContext
    {
        private const string ZendeskApiUrl = "https://coresoftwarestudio.zendesk.com/";


        public ZendeskRestContext()
            : base(ZendeskApiUrl)
        {
            Authenticator = new HttpBasicAuthenticator("PostgresqlDemo@gmail.com", "12345678x@X");
        }

        public RequestRest Requests => Rest<RequestRest>();
    }
}
