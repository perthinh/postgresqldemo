namespace PostgresqlDemo.Rest.Zendesk.Dtos
{
    public class CommentDto
    {
        public string Value { get; set; }
    }
}