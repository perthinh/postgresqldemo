﻿using PostgresqlDemo.Rest.Infrastructure;
using PostgresqlDemo.Rest.Zendesk.Dtos;
using RestSharp;

namespace PostgresqlDemo.Rest.Zendesk.Api
{
    public class RequestRest : RestApiBase
    {
        public RequestRest(RestClient restClient)
            : base(restClient, "/api/v2")
        {
        }

        public void PostRequest(NewTicketDto ticketDto)
        {
           ExecutePost("tickets.json", ticketDto);
        }
    }
}
