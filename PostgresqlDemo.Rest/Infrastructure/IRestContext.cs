namespace PostgresqlDemo.Rest.Infrastructure
{
    public interface IRestContext
    {
        T Rest<T>() where T : class;
    }
}