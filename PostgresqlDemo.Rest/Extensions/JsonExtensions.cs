﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace PostgresqlDemo.Rest.Extensions
{
    public static class JsonExtensions
    {
        public static IList<T> ToObjectList<T>(this JToken token)
        {
            return token.Children()
                .Select(x => x.ToObject<T>())
                .ToList();
        }

    }
}
