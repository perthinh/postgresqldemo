using PostgresqlDemo.DataAccess.Entities;
using Core.DataAccess.Context;
using Core.DataAccess.Identity;

namespace PostgresqlDemo.DataAccess.AspNetIdentity
{
    public class RoleStore : AspNetRoleStore<Role>
    {
        public RoleStore(IDataContext context)
            : base(context)
        {
        }
    }
}