﻿using PostgresqlDemo.DataAccess.Entities;
using Microsoft.AspNet.Identity;

namespace PostgresqlDemo.DataAccess.AspNetIdentity
{
    public class UserManager : UserManager<User, int>
    {
        public UserManager(IUserStore<User, int> store)
            : base(store)
        {
        }
    }
}