﻿using PostgresqlDemo.DataAccess.Entities;
using Microsoft.AspNet.Identity;

namespace PostgresqlDemo.DataAccess.AspNetIdentity
{
    public class RoleManager : RoleManager<Role, int>
    {
        public RoleManager(IRoleStore<Role, int> store)
            : base(store)
        {
        }
    }
}