using PostgresqlDemo.DataAccess.Entities;
using Core.DataAccess.Context;
using Core.DataAccess.Identity;

namespace PostgresqlDemo.DataAccess.AspNetIdentity
{
    public class UserStore : AspNetUserStore<User, Role>
    {
        public UserStore(IDataContext context)
            : base(context)
        {
        }
    }
}