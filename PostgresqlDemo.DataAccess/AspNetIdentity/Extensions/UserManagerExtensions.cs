﻿using System.Threading.Tasks;
using PostgresqlDemo.DataAccess.Entities;
using PostgresqlDemo.DataAccess.Enums;
using Microsoft.AspNet.Identity;

namespace PostgresqlDemo.DataAccess.AspNetIdentity.Extensions
{
    public static class UserManagerExtensions
    {
        public static async Task<IdentityResult> AddToRoleAsync(this UserManager userManager, User user, ERole role)
        {
            return await userManager.AddToRoleAsync(user.Id, role.ToString());
        }
    }
}
