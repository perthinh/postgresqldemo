﻿using Core.DataAccess.Repositories;
using Core.DataAccess.Uow;
using PostgresqlDemo.DataAccess.Entities;

namespace PostgresqlDemo.DataAccess.Repositories
{
    public class PostRepository : Repository<Post>
    {
        public PostRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
