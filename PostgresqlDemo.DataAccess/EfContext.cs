﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using PostgresqlDemo.DataAccess.Entities;
using Core.DataAccess.Context;
using Core.DataAccess.Entities;
using Z.EntityFramework.Plus;

namespace PostgresqlDemo.DataAccess
{
    public class EfContext : DataContext<User, Role>
    {
        static EfContext()
        {
            Database.SetInitializer<EfContext>(null);
        }

        public IDbSet<Post> Posts { get; set; }

        public EfContext()
            : this("DefaultConnection")
        {
        }

        public EfContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            this.Filter<ISoftDelete>(q => q.Where(x => !x.IsDeleted));
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Entity<User>().ToTable("User", "dbo");
            modelBuilder.Entity<Role>().ToTable("Role", "dbo");
            modelBuilder.Entity<Post>().ToTable("Post", "dbo");
        }
    }
}