﻿using Core.Dependency.Behaviors;
using SimpleInjector;
using System;
using System.Diagnostics;

namespace Core.Dependency.Extensions
{
    public static class PropertyInjectionExtensions
    {
        [DebuggerStepThrough]
        public static void AutoWirePropertiesWithAttribute<TAttribute>(this ContainerOptions options)
            where TAttribute : Attribute
        {
            options.PropertySelectionBehavior =
                new AttributePropertyInjectionBehavior(options.PropertySelectionBehavior, typeof(TAttribute));
        }

        [DebuggerStepThrough]
        public static void AutoWirePropertiesWithInjectableAttribute(this ContainerOptions options)
        {
            options.PropertySelectionBehavior = new InjectPropertySelectionBehavior();
        }
    }
}