using SimpleInjector;
using System;
using System.Collections.Generic;

namespace Core.Dependency
{
    public class DefaultIocManager : IIocManager
    {
        private Guid _instanceId;
        private readonly Container _container;

        public DefaultIocManager(Container container)
        {
            _container = container;
            _instanceId = Guid.NewGuid();
        }

        public T GetInstance<T>() where T : class
        {
            return _container.GetInstance<T>();
        }

        public object GetInstance(Type type)
        {
            return _container.GetInstance(type);
        }

        public object GetAllInstances(Type type)
        {
            return _container.GetAllInstances(type);
        }

        public IEnumerable<T> GetAllInstances<T>() where T : class
        {
            return _container.GetAllInstances<T>();
        }

        public object GetContainer()
        {
            return _container;
        }

        public override string ToString()
        {
            return _instanceId.ToString();
        }
    }
}