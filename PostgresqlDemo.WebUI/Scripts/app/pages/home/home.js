/// <reference path="../../../typings/tsd.d.ts" />
var homeCtrl = (function () {
    var ajaxPosttContent;
    return {
        init: init
    };
    function init() {
        ajaxPosttContent = $("#ajax-post-content");
        loadPosts();
    }
    function loadPosts() {
        $.post("/Home/GetPost/")
            .done(function (data) {
            ajaxPosttContent.html("");
            data.forEach(function (item, i) {
                ajaxPosttContent.append("<div class=\"col-md-12\" style='padding:0'>\n                        <div class=\"ibox\" style='margin-bottom:10px!important'>\n                            <div class=\"ibox-content product-box\">\n                                <div class=\"product-desc row\">\n                                    <div class='col-md-3'>\n                                        <img src='" + item.Image + "' style='height:200px'></img>\n                                    </div>\n                                    <div class='col-md-9'>\n                                        <a href=\"#\" class=\"product-name\"> " + item.Title + "</a>\n                                        <div class=\"small m-t-xs\">\n                                            " + item.Content + "\n                                        </div>\n                                        <div class=\"m-t text-righ\">\n                                            <a href=\"#\" class=\"btn btn-xs btn-outline btn-primary\">Info <i class=\"fa fa-long-arrow-right\"></i> </a>\n                                        </div>\n                                    </div>\n                                </div>\n                            </div>\n                        </div>\n                    </div>");
            });
        })
            .fail(function (e) {
            AjaxHelper.fail(e);
        });
    }
})();
$(function () {
    homeCtrl.init();
});
