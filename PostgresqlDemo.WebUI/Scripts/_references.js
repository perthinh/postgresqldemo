/// <autosync enabled="true" />
/// <reference path="../gulpfile.js" />
/// <reference path="app/app.js" />
/// <reference path="app/app_blank.js" />
/// <reference path="app/core/alert.js" />
/// <reference path="app/core/helpers.js" />
/// <reference path="app/core/modal.js" />
/// <reference path="app/core/notify.js" />
/// <reference path="app/core/validator.js" />
/// <reference path="app/pages/admin/activation.js" />
/// <reference path="app/pages/admin/category.js" />
/// <reference path="app/pages/admin/file-manager.js" />
/// <reference path="app/pages/admin/manage-user.js" />
/// <reference path="app/pages/home/contact.js" />
/// <reference path="app/pages/home/home.js" />
/// <reference path="app/pages/home/map.js" />
/// <reference path="app/pages/home/preloader.js" />
/// <reference path="app/pages/register.js" />
/// <reference path="app/pages/site.js" />
/// <reference path="client.js" />
/// <reference path="home/bootstrap.min.js" />
/// <reference path="home/front-end/jquery.cubeportfolio.js" />
/// <reference path="home/front-end/portfolio-1.js" />
/// <reference path="home/jquery.easing.1.3.js" />
/// <reference path="home/jquery.iconmenu.js" />
/// <reference path="home/jquery.magnific-popup.js" />
/// <reference path="home/jquery-2.2.4.min.js" />
/// <reference path="home/main.js" />
/// <reference path="home/swiper.jquery.min.js" />
/// <reference path="home/swiper.min.js" />
/// <reference path="home/theme_map.js" />
/// <reference path="home/vendor.js" />
/// <reference path="home/zendesk.js" />
/// <reference path="kendo/kendo.all.min.js" />
/// <reference path="kendo/kendo.aspnetmvc.min.js" />
/// <reference path="ladda.js" />
/// <reference path="libs/bootstrap.min.js" />
/// <reference path="libs/clipboard.min.js" />
/// <reference path="libs/es6-promise.min.js" />
/// <reference path="libs/ie8.polyfils.min.js" />
/// <reference path="libs/jqbootstrapvalidation-1.3.7.min.js" />
/// <reference path="libs/jquery.min.js" />
/// <reference path="libs/ladda.jquery.min.js" />
/// <reference path="libs/spin.min.js" />
/// <reference path="libs/sweetalert2.min.js" />
/// <reference path="libs/toastr.min.js" />
/// <reference path="plugins/metismenu/metismenu.min.js" />
/// <reference path="plugins/pace/pace.min.js" />
/// <reference path="plugins/slimscroll/jquery.slimscroll.min.js" />
/// <reference path="plugins/wow/wow.min.js" />
