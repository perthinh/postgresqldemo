﻿/// <reference path="../../typings/tsd.d.ts" />
var registerCtrl = (() => {
    var modalTerm: Modal;
    var isModalLoaded: boolean;

    return {
        init: init,
        showTerm: showTerm
    };
    ///////////////
    function init() {
        modalTerm = new Modal("Term of use");
        modalTerm.scrollable();

        Validator.default();
    }

    function showTerm() {
        modalTerm.toggle();
        if (isModalLoaded) return;

        modalTerm.showLoading();

        $.post("/Account/TermOfUse")
            .done(html => {
                modalTerm.setContent(html);
                isModalLoaded = true;
            })
            .fail(e => {
                modalTerm.setContent(AjaxHelper.getError(e));
            })
            .always(() => {
                modalTerm.hideLoading();
            });
    }
})();

$(() => {
    registerCtrl.init();
});