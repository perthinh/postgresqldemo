﻿// Preloader
$(window).on('load', () => {
    $('.preloader-wave-effect').fadeOut();
    $('#preloader-wrapper').delay(100).fadeOut('slow');
});