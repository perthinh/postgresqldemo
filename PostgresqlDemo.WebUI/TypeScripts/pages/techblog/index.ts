﻿/// <reference path="../../../typings/tsd.d.ts" />
var techblogCtrl = (() => {
    var ajaxBlogtContent: any;

    return {
        init: init
    };

    function init() {
        ajaxBlogtContent = $("#ajaxBlogtContent");

        loadBlog();
    }

    function loadBlog() {
        $.post("/en/TechBlog/BlogPartial/")
            .done(html => {
                ajaxBlogtContent.html(html);
            })
            .fail(e => {
                AjaxHelper.fail(e);
            });
    }
})();

$(() => {
    techblogCtrl.init();
});