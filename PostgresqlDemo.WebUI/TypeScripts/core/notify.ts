﻿class Notify {
    private static instance = new Notify();

    static show(type: string, message: string, title: string = "") {
        Notify.instance.show(type, message, title);
    }

    static success(message: string, title: string = "") {
        Notify.instance.success(message, title);
    }

    static warning(message: string, title: string = "") {
        Notify.instance.warning(message, title);
    }

    static info(message: string, title: string = "") {
        Notify.instance.info(message, title);
    }

    static error(message: string, title: string = "") {
        Notify.instance.error(message, title);
    }

    constructor() {
        toastr.options = {
            closeButton: true,
            debug: false,
            progressBar: true,
            preventDuplicates: false,
            positionClass: "toast-bottom-right",
            onclick: null,
            showDuration: 400,
            hideDuration: 1000,
            timeOut: 5000,
            extendedTimeOut: 1000,
            showEasing: "swing",
            hideEasing: "linear",
            showMethod: "fadeIn",
            hideMethod: "fadeOut"
        };
    }

    show(type: string, message: string, title: string = "") {
        toastr[type](message, title);
    }

    success(message: string, title: string = "") {
        toastr.success(message, title);
    }

    warning(message: string, title: string = "") {
        toastr.warning(message, title);
    }

    info(message: string, title: string = "") {
        toastr.info(message, title);
    }

    error(message: string, title: string = "") {
        toastr.error(message, title);
    }
}
