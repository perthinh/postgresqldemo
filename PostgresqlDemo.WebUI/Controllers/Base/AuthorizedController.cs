using System.Web.Mvc;

namespace PostgresqlDemo.WebUI.Controllers.Base
{
    [Authorize]
    public abstract class AuthorizedController : BaseController
    {
    }

    [Authorize(Roles = "Host")]
    public abstract class HostAuthorizedController : BaseController
    {
    }

    [Authorize(Roles = "Customer")]
    public abstract class CustomerAuthorizedController : BaseController
    {
    }
}