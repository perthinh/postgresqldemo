﻿using System.Data.Entity;
using System.Threading.Tasks;
using System.Web.Mvc;
using PostgresqlDemo.WebCore.Modules.Posts.Services;
using PostgresqlDemo.WebUI.Controllers.Base;

namespace PostgresqlDemo.WebUI.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IPostService _postService;

        public HomeController(IPostService postService)
        {
            _postService = postService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> GetPost()
        {
            return Json(await _postService.GetAll().ToListAsync());
        }
    }
}