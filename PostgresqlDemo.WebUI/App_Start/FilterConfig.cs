﻿using System.Web.Mvc;
using PostgresqlDemo.WebUI.Infrastructure.Filters;
using Core.Web.Mvc.Filters;

namespace PostgresqlDemo.WebUI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ElmahHandledErrorFilter());
            filters.Add(new AjaxHandledErrorFilter());
            //filters.Add(new UserAuditAttribute());
        }
    }
}
