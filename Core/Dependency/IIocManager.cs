using System;
using System.Collections.Generic;

namespace Core.Dependency
{
    public interface IIocManager
    {
        T GetInstance<T>() where T : class;
        object GetInstance(Type type);
        object GetAllInstances(Type type);
        IEnumerable<T> GetAllInstances<T>() where T : class;
        object GetContainer();
    }
}