﻿using System;
using System.Runtime.Serialization;

namespace Core.Exceptions
{
    [Serializable]
    public class UserFriendlyException : SysException
    {
        public string Details { get; private set; }

        public int Code { get; set; }

        public UserFriendlyException()
        {

        }

        public UserFriendlyException(SerializationInfo serializationInfo, StreamingContext context)
            : base(serializationInfo, context)
        {

        }

        public UserFriendlyException(string message)
            : base(message)
        {

        }

        public UserFriendlyException(int code, string message)
            : this(message)
        {
            Code = code;
        }

        public UserFriendlyException(string message, string details)
            : base(message)
        {
            Details = details;
        }

        public UserFriendlyException(int code, string message, string details)
            : this(message, details)
        {
            Code = code;
        }

        public UserFriendlyException(string message, Exception innerException)
            : base(message, innerException)
        {

        }

        public UserFriendlyException(string message, string details, Exception innerException)
            : base(message, innerException)
        {
            Details = details;
        }
    }
}