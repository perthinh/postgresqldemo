﻿namespace Core.Runtime.Security
{
    public static class CustomClaimTypes
    {
        public const string TenantId = "http://tempuri.org/identity/claims/tenantId";
        public const string ImpersonatorUserId = "http://tempuri.org/identity/claims/impersonatorUserId";
        public const string ImpersonatorTenantId = "http://tempuri.org/identity/claims/impersonatorTenantId";

    }
}
