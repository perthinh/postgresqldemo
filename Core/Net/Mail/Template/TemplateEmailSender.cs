﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Exceptions;

namespace Core.Net.Mail.Template
{
    public class TemplateEmailSender : ITemplateEmailSender
    {
        private readonly IEmailSender _emailSender;
        private readonly IEmailTemplateService _templateService;

        public TemplateEmailSender(IEmailSender emailSender, IEmailTemplateService templateService)
        {
            _emailSender = emailSender;
            _templateService = templateService;
        }

        public async Task SendEmailAsync(string template, string to, string subject, IDictionary<string, string> replacements = null)
        {
            var body = await _templateService.Get(template);

            if (string.IsNullOrEmpty(body)) throw new BusinessException($"Cannot find email template {template}");

            if (replacements != null)
            {
                foreach (var kvp in replacements)
                {
                    body = body.Replace($"%{kvp.Key}%", kvp.Value);
                }
            }
            await _emailSender.SendAsync(to, subject, body);
        }
    }
}
