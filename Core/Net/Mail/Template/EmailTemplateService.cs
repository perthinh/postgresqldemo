﻿using System.IO;
using System.Threading.Tasks;
using System.Web.Hosting;
using Core.Configuration;
using Core.IO;

namespace Core.Net.Mail.Template
{
    public class EmailTemplateService : IEmailTemplateService
    {
        private readonly ISettingManager _settings;

        public EmailTemplateService(ISettingManager settings)
        {
            _settings = settings;
        }

        public Task<string> Get(string name)
        {
            if (!Path.HasExtension(name))
            {
                name = $"{name}.html";
            }
            var root = HostingEnvironment.MapPath(_settings.GetNotEmptyValue("Mail.TemplatePath"));
            // ReSharper disable once AssignNullToNotNullAttribute
            return FileHelper.ReadTextAsync(Path.Combine(root, name));
        }
    }
}