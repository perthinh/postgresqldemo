﻿using System.Threading.Tasks;

namespace Core.Net.Mail.Template
{
    public interface IEmailTemplateService
    {
        Task<string> Get(string name);
    }
}