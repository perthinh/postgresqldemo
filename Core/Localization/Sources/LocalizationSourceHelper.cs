﻿using Core.Dependency;
using Core.Exceptions;
using Humanizer;
using Serilog;
using System;

namespace Core.Localization.Sources
{
    public static class LocalizationSourceHelper
    {
        private static ILocalizationConfig Config => DependencyManager.GetInstance<ILocalizationConfig>();

        public static string ReturnGivenNameOrThrowException(string name)
        {
            var exceptionMessage = $"Can not find '{name}' in localization source!";

            if (Config.TitleizeGivenTextIfNotFound)
            {
                return name.Substring(name.IndexOf("_", StringComparison.Ordinal) + 1).Titleize();
            }

            if (!Config.ReturnGivenTextIfNotFound)
            {
                throw new SysException(exceptionMessage);
            }
            Log.Warning(exceptionMessage);
            return Config.WrapGivenTextIfNotFound ? $"[{name}]" : name;
        }

        public static string GenerateCommonName(string propertyName, string metadataName)
        {
            return GenerateModelName("_", propertyName, metadataName);
        }

        public static string GenerateModelName(Type containerType, string propertyName, string metadataName)
        {
            return GenerateModelName(containerType.Name, propertyName, metadataName);
        }

        private static string GenerateModelName(string prefix, string propertyName, string metadataName)
        {
            var name = $"{prefix}_{propertyName}";
            if (!string.IsNullOrWhiteSpace(metadataName))
            {
                name = $"{name}_{metadataName}";
            }
            return name;
        }
    }
}