﻿using System.Collections.Generic;

namespace Core.Localization
{
    public interface ILanguageProvider
    {
        IList<LanguageInfo> Languages { get; }
    }
}