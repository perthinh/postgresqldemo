﻿using Core.Exceptions;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;

namespace Core.Localization
{
    public class LanguageManager : ILanguageManager
    {
        private readonly IReadOnlyList<LanguageInfo> _languages;
        public LanguageInfo CurrentLanguage => GetCurrentLanguage();

        public LanguageManager(ILocalizationConfig config)
        {
            _languages = new ReadOnlyCollection<LanguageInfo>(config.LanguageProvider.Languages);
        }

        public IReadOnlyList<LanguageInfo> GetLanguages()
        {
            return _languages;
        }

        private LanguageInfo GetCurrentLanguage()
        {
            if (_languages.Count <= 0)
            {
                throw new SysException("No language defined in this application.");
            }

            var currentCultureName = Thread.CurrentThread.CurrentUICulture.Name;

            //Try to find exact match
            var currentLanguage = _languages.FirstOrDefault(l => l.Name == currentCultureName);
            if (currentLanguage != null)
            {
                return currentLanguage;
            }

            //Try to find best match
            currentLanguage = _languages.FirstOrDefault(l => currentCultureName.StartsWith(l.Name));
            if (currentLanguage != null)
            {
                return currentLanguage;
            }

            //Try to find default language
            currentLanguage = _languages.FirstOrDefault(l => l.IsDefault);
            if (currentLanguage != null)
            {
                return currentLanguage;
            }

            //Get first one
            return _languages[0];
        }

    }
}