﻿using Core.Auditing;
using Core.Localization;
using Core.Net.Mail;

namespace Core.Configuration.Startup
{
    public interface ISiteConfig
    {
        ISettingManager Settings { get; }
        ILocalizationConfig Localization { get; }
        IEmailSender EmailSender { get; }
        IAuditingConfig Auditing { get; }
    }
}
