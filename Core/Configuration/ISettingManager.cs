﻿namespace Core.Configuration
{
    public interface ISettingManager
    {
        string GetNotEmptyValue(string name);
        string GetValue(string name);
        T GetValue<T>(string name, T defaultValue = default(T)) where T : struct;
        T GetNotEmptyValue<T>(string name) where T : struct;
    }
}
