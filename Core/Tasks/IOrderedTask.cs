namespace Core.Tasks
{
    public interface IOrderedTask
    {
        int Order { get; }
    }
}