﻿
using System.Threading.Tasks;

namespace Core.Tasks
{
    public interface IRunAtInit : IOrderedTask
    {
        Task Execute();
    }
}
