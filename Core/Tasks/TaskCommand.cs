﻿using System.Linq;
using Core.Dependency;

namespace Core.Tasks
{
    public static class TaskCommand
    {
        public static void ExecuteInitTasks()
        {
            foreach (var task in DependencyManager.Current.GetAllInstances<IRunAtInit>().OrderBy(x => x.Order))
            {
                task.Execute();
            }
        }
    }
}
