namespace Core.Tasks
{
    public class OrderedTask : IOrderedTask
    {
        public virtual int Order { get; }

        public OrderedTask(int order = 0)
        {
            Order = order;
        }
    }
}