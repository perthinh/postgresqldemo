﻿
using System.Collections;
using System.Collections.Specialized;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace Core.Extensions
{
    public static class HttpRequestMessageExtensions
    {
        private const string HttpContext = "MS_HttpContext";
        private const string RemoteEndpointMessage = "System.ServiceModel.Channels.RemoteEndpointMessageProperty";

        public static string GetClientIpAddress(this HttpRequestMessage request)
        {
            if (request.Properties.ContainsKey(HttpContext))
            {
                dynamic ctx = request.Properties[HttpContext];
                if (ctx != null)
                {
                    return ctx.Request.UserHostAddress;
                }
            }

            if (request.Properties.ContainsKey(RemoteEndpointMessage))
            {
                dynamic remoteEndpoint = request.Properties[RemoteEndpointMessage];
                if (remoteEndpoint != null)
                {
                    return remoteEndpoint.Address;
                }
            }

            return string.Empty;
        }

        public static string GetBrowserInfo(this HttpRequestMessage request)
        {
            if (!request.Headers.Contains("User-Agent"))
            {
                return string.Empty;
            }

            var userAgent = request.Headers.GetValues("User-Agent").ToString();
            if (string.IsNullOrWhiteSpace(userAgent)) return "";

            var userBrowser = new HttpBrowserCapabilities
            {
                Capabilities = new Hashtable { [""] = userAgent }
            };
            var factory = new BrowserCapabilitiesFactory();
            factory.ConfigureBrowserCapabilities(new NameValueCollection(), userBrowser);
            var sb = new StringBuilder();
            sb.AppendFormat("MobileDevice={0};", userBrowser.IsMobileDevice);
            sb.AppendFormat("Browser={0};", userBrowser.Browser);
            sb.AppendFormat("Version={0};", userBrowser.Version);
            return sb.ToString();
        }
    }
}
