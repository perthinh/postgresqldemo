﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Infrastructure.Paging
{
    /// <summary>
    /// Represents a subset of a collection of objects that can be individually accessed by index and containing metadata about the superset collection of objects this subset was created from.
    /// </summary>
    /// <remarks>
    /// Represents a subset of a collection of objects that can be individually accessed by index and containing metadata about the superset collection of objects this subset was created from.
    /// </remarks>
    /// <typeparam name="T">The type of object the collection should contain.</typeparam>
    /// <seealso cref="IPagedList{T}"/>
    /// <seealso cref="BasePagedList{T}"/>
    /// <seealso cref="StaticPagedList{T}"/>
    /// <seealso cref="List{T}"/>
    [Serializable]
    public class PagedList<T> : BasePagedList<T>
    {
        public PagedList(IQueryable<T> superset, int pageNumber, int pageSize, int? totalCount = null)
        {
            if (pageNumber < 1)
                throw new ArgumentOutOfRangeException(nameof(pageNumber), pageNumber, "PageNumber cannot be below 1.");
            if (pageSize < 1)
                throw new ArgumentOutOfRangeException(nameof(pageSize), pageSize, "PageSize cannot be less than 1.");

            // set source to blank list if superset is null to prevent exceptions
            TotalItemCount = totalCount ?? (superset?.Count() ?? 0);

            PageSize = pageSize;
            PageNumber = pageNumber;
            PageCount = TotalItemCount > 0
                        ? (int)Math.Ceiling(TotalItemCount / (double)PageSize)
                        : 0;
            HasPreviousPage = PageNumber > 1;
            HasNextPage = PageNumber < PageCount;
            IsFirstPage = PageNumber == 1;
            IsLastPage = PageNumber >= PageCount;
            FirstItemOnPage = (PageNumber - 1) * PageSize + 1;
            var numberOfLastItemOnPage = FirstItemOnPage + PageSize - 1;
            LastItemOnPage = numberOfLastItemOnPage > TotalItemCount
                            ? TotalItemCount
                            : numberOfLastItemOnPage;

            // add items to internal list
            if (superset != null && TotalItemCount > 0)
            {
                Subset.AddRange(pageNumber == 1
                        ? superset.Skip(0).Take(pageSize).ToList()
                        : superset.Skip((pageNumber - 1)*pageSize).Take(pageSize).ToList()
                );
            }
        }

        public PagedList(IEnumerable<T> superset, IPagedList metadata)
        {
            TotalItemCount = metadata.TotalItemCount;

            PageSize = metadata.PageSize;
            PageNumber = metadata.PageNumber;
            PageCount = metadata.PageCount;

            HasPreviousPage = metadata.HasPreviousPage;
            HasNextPage = metadata.HasNextPage;
            IsFirstPage = metadata.IsFirstPage;
            IsLastPage = metadata.IsLastPage;
            FirstItemOnPage = metadata.FirstItemOnPage;
            LastItemOnPage = metadata.LastItemOnPage;

            Subset.AddRange(superset);
        }

        public PagedList(IEnumerable<T> superset, int pageNumber, int pageSize, int? totalCount = null)
            : this(superset.AsQueryable<T>(), pageNumber, pageSize, totalCount)
        {
        }

        public PagedList(int pageNumber, int pageSize)
            : this(new List<T>(), pageNumber, pageSize)
        {
        }
    }
}