﻿namespace Core.Auditing
{
    public class AuditingConfig : IAuditingConfig
    {
        public bool IsEnabled { get; set; }
        public bool IsEnabledForAnonymousUsers { get; set; }

        public AuditingConfig()
        {
            IsEnabled = true;
            IsEnabledForAnonymousUsers = true;
        }
    }
}