﻿using System;

namespace Core.DataAccess.Entities
{
    public interface IHasCreationTime
    {
        DateTime CreatedDate { get; set; }
    }

    public interface IHasAuditedTime : IHasCreationTime
    {
        DateTime UpdatedDate { get; set; }
    }

    public interface ICreationAudited
    {
        DateTime CreatedDate { get; set; }
        int CreatedBy { get; set; }
    }

    public interface IAuditedEntity : ICreationAudited
    {
        DateTime UpdatedDate { get; set; }

        int UpdatedBy { get; set; }
    }

    public abstract class CreationAudited<T> : Entity<T>, ICreationAudited
    {
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
    }

    public abstract class AuditedEntity<T> : CreationAudited<T>, IAuditedEntity
    {
        public DateTime UpdatedDate { get; set; }

        public int UpdatedBy { get; set; }
    }
}