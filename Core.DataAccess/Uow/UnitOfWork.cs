﻿using Core.DataAccess.Context;
using Core.DataAccess.Utils;
using CuttingEdge.Conditions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;

namespace Core.DataAccess.Uow
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly Guid _instanceId;
        private bool _disposed;
        private Dictionary<Type, dynamic> _repositories;
        private DbContextTransaction _transaction;
        private readonly object _lock = new object();

        protected Dictionary<Type, dynamic> Repositories => _repositories ?? (_repositories = new Dictionary<Type, dynamic>());

        public IDataContext DataContext { get; private set; }

        public UnitOfWork(IDataContext dataContext)
        {
            Condition.Requires(dataContext, nameof(dataContext)).IsNotNull();
            DataContext = dataContext;
            _instanceId = Guid.NewGuid();
        }

        public T Repo<T>() where T : class
        {
            var type = typeof(T);
            if (!Repositories.ContainsKey(type))
            {
                lock (_lock)
                {
                    if (!Repositories.ContainsKey(type))
                    {
                        Repositories.Add(type, DbUtil.Repository<T>(this));
                    }
                }
            }
            return Repositories[type];
        }


        public int SaveChanges()
        {
            return DataContext.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await DataContext.SaveChangesAsync();
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return await DataContext.SaveChangesAsync(cancellationToken);
        }

        #region Unit of Work Transactions

        public void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified)
        {
            var dbContext = DataContext as DbContext;
            if (dbContext != null)
            {
                _transaction = dbContext.Database.BeginTransaction(isolationLevel);
            }
        }

        public void Commit()
        {
            _transaction?.Commit();
        }

        public void Rollback()
        {
            _transaction?.Rollback();
        }

        #endregion
        public override string ToString()
        {
            // Used for debug
            return _instanceId.ToString();
        }

        #region Destructors

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~UnitOfWork()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    DataContext.Dispose();
                    DataContext = null;
                }
                _disposed = true;
            }
        }
        #endregion
    }
}