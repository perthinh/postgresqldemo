using Core.DataAccess.Context;
using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace Core.DataAccess.Uow
{
    public interface IUnitOfWork : IDisposable
    {
        IDataContext DataContext { get; }
        T Repo<T>() where T : class;
        int SaveChanges();
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        void BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.Unspecified);
        void Commit();
        void Rollback();
    }
}