﻿using Core.DataAccess.Context;
using Core.DataAccess.Entities;
using Core.DataAccess.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace Core.DataAccess.Infrastructure
{
    internal sealed class EntityKeyHelper
    {
        private static readonly Lazy<EntityKeyHelper> LazyInstance =
            new Lazy<EntityKeyHelper>(() => new EntityKeyHelper());

        private readonly IDictionary<Type, string[]> _cachedKeys = new Dictionary<Type, string[]>();

        private EntityKeyHelper()
        {
        }

        public static EntityKeyHelper Instance
        {
            get { return LazyInstance.Value; }
        }

        public string[] GetKeyMembers<T>(IDataContext context) where T : class
        {
            var entityType = GetBaseType<T>();

            if (!_cachedKeys.ContainsKey(entityType))
            {
                _cachedKeys.Add(entityType, GetEntityKeyNames(entityType, context));
            }
            return _cachedKeys[entityType];
        }

        public string[] GetEntityKeyNames(Type type, IDataContext context)
        {
            var objectContextAdapter = context as IObjectContextAdapter;

            if (objectContextAdapter != null)
            {
                var objectContext = objectContextAdapter.ObjectContext;
                string entityTypeName = string.Format("{0}.{1}", context.GetType().Namespace, type.Name);
                var entityType = objectContext.MetadataWorkspace.GetItem<EntityType>(entityTypeName, DataSpace.CSpace);
                return entityType.KeyProperties.Select(k => k.Name).ToArray();
            }

            throw new ArgumentException(string.Format("No primary key field(s) in the type '{0}'.", type.FullName));
        }

        private static Type GetBaseType<T>()
        {
            var entityType = typeof(T);
            var entityBaseTypes = new[] {
                typeof(AuditedEntity<>), typeof(Entity<>), typeof(EntityBase),
                typeof(AspNetUser), typeof(AspNetRole)};

            while (entityType != typeof(object))
            {
                var baseType = entityType.BaseType;
                if (baseType == null) break;

                var current = baseType.IsGenericType ? baseType.GetGenericTypeDefinition() : baseType;
                if (entityBaseTypes.Contains(current)) break;

                entityType = baseType;
            }
            return entityType;
        }

    }
}