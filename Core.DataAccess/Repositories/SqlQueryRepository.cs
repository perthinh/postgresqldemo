﻿using Core.DataAccess.Uow;
using CuttingEdge.Conditions;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Core.DataAccess.Repositories
{
    public class SqlQueryRepository
    {
        protected readonly IUnitOfWork TheUnitOfWork;

        protected DbContext DbContext { get; }

        public Database Database => DbContext.Database;

        public SqlQueryRepository(IUnitOfWork unitOfWork)
        {
            Condition.Requires(unitOfWork, nameof(unitOfWork)).IsNotNull();
            Condition.Requires(unitOfWork.DataContext, nameof(unitOfWork.DataContext)).IsNotNull();

            // Not use for saving but repository factory
            TheUnitOfWork = unitOfWork;

            // Only support DbContext
            DbContext = (DbContext)TheUnitOfWork.DataContext;
        }

        public IList<T> GetList<T>(string sql, params object[] parameters)
        {
            return Database.SqlQuery<T>(sql, parameters).ToList();
        }

        public T SingleOrDefault<T>(string sql, params object[] parameters)
        {
            return Database.SqlQuery<T>(sql, parameters).SingleOrDefault();
        }

        public int ExecuteSqlCommand(string sql, params object[] parameters)
        {
            return Database.ExecuteSqlCommand(sql, parameters);
        }
    }
}
